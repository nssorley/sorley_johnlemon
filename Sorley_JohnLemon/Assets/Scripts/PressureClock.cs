﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading;

public class PressureClock : MonoBehaviour
{
    public int timeLeft = 200;
    public Text countdown;
    public Text pressuretext;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("LoseTime");
        Time.timeScale = 1;
        pressuretext.text = ("Get the treasure and back out! Be quick Now! The buyers are waiting.");
    }

    // Update is called once per frame
    void Update()
    {
        countdown.text = ("" + timeLeft);
        if (timeLeft == 100)
        {
            pressuretext.text = ("Hurry! The buyers cant wait long");
        }
        if (timeLeft == 50)
        {
            pressuretext.text = ("Come with what you have! Just get out!");
        }
        if (timeLeft == 25)
        {
            pressuretext.text = ("Come back please! They are almost here!");
        }
        if (timeLeft == 10)
        {
            pressuretext.text = ("Oh no....... theyre here");
        }
    }

    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
        }
    }
}
