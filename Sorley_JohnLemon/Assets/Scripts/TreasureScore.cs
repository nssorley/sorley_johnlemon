﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TreasureScore : MonoBehaviour
{
    GameEnding ge;

    public Text treasurescore;

    // Start is called before the first frame update
    void Start()
    {
        GameObject GameEnding = GameObject.FindGameObjectWithTag("Finish");
        ge = GameEnding.GetComponent<GameEnding>();
    }

    // Update is called once per frame
    void Update()
    {
        treasurescore.text = ("You got," + ge.m_treasureCollected + " treasure. Congratulations!");
    }
}
